# Deploy

Copy the example files.

```
cp group_vars/all/vars.example group_vars/all/vars
cp hosts.ini.example hosts.ini
```

Edit the files as needed and run:

```
ansible-playbook --ask-vault-pass -K -i inventory.ini site.yaml
```