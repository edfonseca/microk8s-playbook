#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters"
    echo "usage: $0 <version> <namespace>"
    exit 1
fi

version="$1"
namespace="$2"

curl -L https://github.com/jetstack/cert-manager/releases/download/${version}/cert-manager.crds.yaml | \
    sed "s#'cert-manager/#'${namespace}/#" | \
    sed "s#namespace: 'cert-manager'#namespace: '${namespace}'#" | \
    kubectl apply --validate=false -f -
